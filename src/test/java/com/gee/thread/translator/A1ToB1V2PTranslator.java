package com.gee.thread.translator;

import com.gee.thread.VariableDemo;
import com.gee.thread.flow.executor.ExecuteContext;
import com.gee.thread.flow.common.result.ExecuteState;
import com.gee.thread.flow.translator.V2PTranslator;
import com.gee.thread.flow.work.AbstractWork;

import java.util.Map;

/**
 * desc:
 *
 * @author gee wrote on  2021-01-16 21:12:25
 */
public class A1ToB1V2PTranslator implements V2PTranslator<VariableDemo,Integer> {

    @Override
    public Integer translate(AbstractWork<VariableDemo, Integer, ?> abstractWork,
                             ExecuteContext<VariableDemo> executeContext) {
        if (executeContext.getWorkExecuteState("workA1").get() == ExecuteState.SUCCESSFUL.getCode()){
            String workB1Result = executeContext.getResult("workA1", String.class);
            return executeContext.getVariable().getInteger() + Integer.parseInt(workB1Result);
        }
        return null;
    }
}
