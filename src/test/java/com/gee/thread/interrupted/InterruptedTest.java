package com.gee.thread.interrupted;

import com.gee.thread.*;
import com.gee.thread.flow.executor.ExecuteContext;
import com.gee.thread.flow.executor.WorkExecuteDesigner;
import com.gee.thread.flow.executor.WorkExecutor;
import com.gee.thread.translator.A1V2PTranslator;
import com.gee.thread.translator.C2V2PTranslator;
import com.gee.thread.translator.DV2PTranslator1;

import java.util.concurrent.TimeUnit;

/**
 * desc:
 *
 * @author gee wrote on  2021-01-23 06:29:16
 */
public class InterruptedTest {

    /**
     * 执行线程                        A ---
     *            enableInterruptedWork ---  D
     *            UnableInterruptedWork ---
     * enableInterruptedWork 执行时间为2秒,对于D单元非必须
     * UnableInterruptedWork 执行时间为2秒,对于D单元非必须
     * A单元执行时间1秒 对于D单元必须, 会异常
     * enableInterruptedWork 单元可以被中断, UnableInterruptedWork不可被中断
     * D 单元执行0.5秒
     *
     * 预期:  A单元在执行1秒之后异常, D单元因为A单位为必须项而直接结束
     * 并设置前置单元未执行完且可以被中断的enableInterruptedWork单元中断执行
     * UnableInterruptedWork单元因不可被中断而继续执行.
     */
    private static void testOneNecessaryException() throws InterruptedException {
        VariableDemo variableDemo = new VariableDemo("str", "str2", 100, 1000.0);

        WorkExceptionA1<VariableDemo> workExceptionA1 = WorkExceptionA1.build("workExceptionA1", new A1V2PTranslator());
        EnableInterruptedWork<VariableDemo> enableInterruptedWork = EnableInterruptedWork.build("enableInterruptedWork", new C2V2PTranslator());
        UnableInterruptedWork<VariableDemo> unableInterruptedWork = UnableInterruptedWork.build("unableInterruptedWork", new C2V2PTranslator());
        WorkD<VariableDemo> workD = WorkD.build("workD", new DV2PTranslator1());

        WorkExecuteDesigner<VariableDemo> workExecuteDesigner = WorkExecuteDesigner.<VariableDemo>builder().variable(variableDemo)
                .newStartWork(workExceptionA1)
                .next(true, workD, null)
                .newStartWork(enableInterruptedWork)
                .enableInterrupted(true)
                .next(false, workD, null)
                .newStartWork(unableInterruptedWork)
                .next(false, workD, null)
                .currentWork(workD)
                .build();
        long start = System.currentTimeMillis();
        System.out.println(start);
        ExecuteContext<?> executeContext = WorkExecutor.execute(workExecuteDesigner, WorkExecutor.COMMON_POOL,1900, TimeUnit.MILLISECONDS);
//        ExecuteContext<?> executeContext = WorkExecutor.execute(workExecuteDesigner, WorkExecutor.COMMON_POOL,21000000, TimeUnit.MILLISECONDS);

        System.out.println(System.currentTimeMillis() - start);
        System.out.println(executeContext);
        WorkExecutor.shutdown();
    }

    public static void main(String[] args) throws InterruptedException {
        testOneNecessaryException();
    }
}
