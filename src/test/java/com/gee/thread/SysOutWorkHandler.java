package com.gee.thread;

import com.gee.thread.flow.common.result.ExecuteState;
import com.gee.thread.flow.executor.ExecuteContext;
import com.gee.thread.flow.handler.DefaultWorkHandler;
import com.gee.thread.flow.work.AbstractWork;

import java.util.Map;

/**
 * desc:
 *
 * @author gee wrote on  2021-02-17 08:48:41
 */
public class SysOutWorkHandler<V> extends DefaultWorkHandler<V> {
    @Override
    public void postBeforeBegin(String currentWorkId, Object param) {
        System.out.println(System.currentTimeMillis() + ": " + currentWorkId + " postBeforeBegin, param: " + param);
    }

    @Override
    protected void postAfterFinishBeforeNextBegin(AbstractWork<V,?,?> currentWork, ExecuteState currentWorkExecuteState,
                                                  ExecuteContext<V> executeContext) {
        System.out.println(System.currentTimeMillis() + ": " + currentWork.getId() + " postAfterFinishBeforeNextBegin, "
                + " currentWorkExecuteState: " + currentWorkExecuteState
                + " result: " + executeContext.getWorkResult(currentWork.getId()));
    }

    @Override
    protected void postAfterFinishAfterNextBegin(AbstractWork<V,?,?> currentWork, ExecuteContext<V> executeContext) {
        System.out.println(System.currentTimeMillis() + ": " + currentWork.getId() + " postAfterFinishAfterNextBegin, "
                + " result: " + executeContext.getWorkResult(currentWork.getId()));
    }
}
