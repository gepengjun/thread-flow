package com.gee.thread.condition.gaode;

import com.gee.thread.flow.translator.V2PTranslator;
import com.gee.thread.flow.work.AbstractWork;

import java.util.Random;

/**
 * desc:
 *
 * @author gee
 * @since 2021-03-19 09:20:21
 */
public class GaoDeWork<V> extends AbstractWork<V,GaoDeRequest,GaoDeResponse> {

    private String id;
    @Override
    public GaoDeResponse process(GaoDeRequest param) throws Exception {
        Thread.sleep(1000);
        int a = new Random().nextInt(4);
        System.out.println("GaoDeWork  a:" + a);
        if (a % 3 == 0){
            return new GaoDeResponse("0", "调用成功", "117.32", "54.23");
        }else if ( a % 3 == 1) {
            return new GaoDeResponse("1", "调用失败", null, null);
        }else {
            int b = 1 / 0;
            return null;
        }
    }

    @Override
    public String getId() {
        return id;
    }

    private GaoDeWork(String id) {
        this.id = id;
    }

    public static <V> GaoDeWork<V> build(String id, V2PTranslator<V,GaoDeRequest> v2PTranslator){
        GaoDeWork<V> instance = new GaoDeWork<>(id);
        instance.setV2PTranslator(v2PTranslator);
        return instance;
    }
}
