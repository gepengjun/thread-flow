package com.gee.thread.condition.gaode;

/**
 * desc:
 *
 * @author gee
 * @since 2021-03-19 09:22:58
 */
public class GaoDeRequest {

    private String address;

    public GaoDeRequest(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "GaoDeRequest{" +
                "address='" + address + '\'' +
                '}';
    }
}
