package com.gee.thread.condition.gaode;

import com.gee.thread.VariableDemo;
import com.gee.thread.flow.executor.ExecuteContext;
import com.gee.thread.flow.translator.V2PTranslator;
import com.gee.thread.flow.work.AbstractWork;

/**
 * desc:
 *
 * @author gee
 * @since 2021-03-19 10:19:40
 */
public class GaoDeV2PTranslator implements V2PTranslator<VariableDemo, GaoDeRequest> {
    @Override
    public GaoDeRequest translate(AbstractWork<VariableDemo, GaoDeRequest, ?> currentWork, ExecuteContext<VariableDemo> executeContext) {
        VariableDemo variable = executeContext.getVariable();
        return new GaoDeRequest(variable.getStr());
    }
}
