package com.gee.thread.condition.gaode;

/**
 * desc:
 *
 * @author gee
 * @since 2021-03-19 09:21:15
 */
public class GaoDeResponse {

    private String resCode;

    private String resMsg;

    private String lat;

    private String lot;

    public GaoDeResponse(String resCode, String resMsg, String lat, String lot) {
        this.resCode = resCode;
        this.resMsg = resMsg;
        this.lat = lat;
        this.lot = lot;
    }

    public String getResCode() {
        return resCode;
    }

    public void setResCode(String resCode) {
        this.resCode = resCode;
    }

    public String getResMsg() {
        return resMsg;
    }

    public void setResMsg(String resMsg) {
        this.resMsg = resMsg;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    @Override
    public String toString() {
        return "GaoDeResponse{" +
                "resCode='" + resCode + '\'' +
                ", resMsg='" + resMsg + '\'' +
                ", lat='" + lat + '\'' +
                ", lot='" + lot + '\'' +
                '}';
    }
}
