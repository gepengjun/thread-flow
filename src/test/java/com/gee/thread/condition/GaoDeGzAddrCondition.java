package com.gee.thread.condition;

import com.gee.thread.condition.gaode.GaoDeResponse;
import com.gee.thread.flow.common.result.ExecuteState;
import com.gee.thread.flow.executor.Condition;

/**
 * desc:
 *    高德成功调用 且 返回码值为"0"
 * @author gee
 * @since 2021-03-19 10:27:44
 */
public class GaoDeGzAddrCondition extends Condition<GaoDeResponse> {

    private static final GaoDeGzAddrCondition CONDITION = new GaoDeGzAddrCondition();

    @Override
    public boolean determine(int executeState, GaoDeResponse gaoDeResponse) {
        return executeState == ExecuteState.SUCCESSFUL.getCode()
                && gaoDeResponse != null
                && "0".equals(gaoDeResponse.getResCode());
    }

    private GaoDeGzAddrCondition(){
    }

    public static GaoDeGzAddrCondition getInstance(){
        return CONDITION;
    }
}
