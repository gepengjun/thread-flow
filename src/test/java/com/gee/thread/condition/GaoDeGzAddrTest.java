package com.gee.thread.condition;

import com.gee.thread.SysOutWorkHandler;
import com.gee.thread.VariableDemo;
import com.gee.thread.WorkA1;
import com.gee.thread.WorkB1;
import com.gee.thread.WorkC1;
import com.gee.thread.condition.gaode.GaoDeV2PTranslator;
import com.gee.thread.condition.gaode.GaoDeWork;
import com.gee.thread.condition.gzaddr.GzAddrV2PTranslator;
import com.gee.thread.condition.gzaddr.GzAddrWork;
import com.gee.thread.flow.executor.ExecuteContext;
import com.gee.thread.flow.executor.WorkExecuteDesigner;
import com.gee.thread.flow.executor.WorkExecutor;
import com.gee.thread.translator.A1ToB1V2PTranslator;
import com.gee.thread.translator.A1V2PTranslator;
import com.gee.thread.translator.B1ToC1V2PTranslator;

import java.util.concurrent.TimeUnit;

/**
 * desc:
 *
 * @author gee
 * @since 2021-03-19 10:39:58
 */
public class GaoDeGzAddrTest {

    private static void test(){
        VariableDemo variableDemo = new VariableDemo("str", 100);

        GaoDeWork<VariableDemo> gaoDeWork = GaoDeWork.build("gaoDeWork", new GaoDeV2PTranslator());
        GzAddrWork<VariableDemo> gzAddrWork = GzAddrWork.build("gzAddrWork", new GzAddrV2PTranslator());

        WorkExecuteDesigner<VariableDemo> workExecuteDesigner = WorkExecuteDesigner.<VariableDemo>builder().variable(variableDemo)
                .commonHandler(new SysOutWorkHandler<>())
                .newStartWork(gaoDeWork)
                .next(true, gzAddrWork, GaoDeGzAddrCondition.getInstance())
                .currentWork(gzAddrWork)
                .build();

        long start = System.currentTimeMillis();

//        ExecuteContext<?> executeContext = WorkExecutor.execute(workExecuteDesigner,2900, TimeUnit.MILLISECONDS);
        ExecuteContext<?> executeContext = WorkExecutor.execute(workExecuteDesigner,3100, TimeUnit.MILLISECONDS);
        System.out.println(System.currentTimeMillis() - start);
        System.out.println(executeContext);
        WorkExecutor.shutdown();
    }

    public static void main(String[] args) {
        test();
    }
}
