package com.gee.thread.condition.gzaddr;

import com.gee.thread.VariableDemo;
import com.gee.thread.condition.gaode.GaoDeResponse;
import com.gee.thread.flow.executor.ExecuteContext;
import com.gee.thread.flow.translator.V2PTranslator;
import com.gee.thread.flow.work.AbstractWork;

/**
 * desc:
 *
 * @author gee
 * @since 2021-03-19 10:25:22
 */
public class GzAddrV2PTranslator implements V2PTranslator<VariableDemo, GzAddrRequest> {
    @Override
    public GzAddrRequest translate(AbstractWork<VariableDemo, GzAddrRequest, ?> currentWork, ExecuteContext<VariableDemo> executeContext) {
        GaoDeResponse gaoDeResponse = executeContext.getResult("gaoDeWork", GaoDeResponse.class);
        return new GzAddrRequest(gaoDeResponse.getLat(), gaoDeResponse.getLot());
    }
}
