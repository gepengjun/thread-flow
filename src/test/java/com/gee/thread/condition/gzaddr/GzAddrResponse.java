package com.gee.thread.condition.gzaddr;

/**
 * desc:
 *
 * @author gee
 * @since 2021-03-19 09:21:15
 */
public class GzAddrResponse {

    private String resCode;

    private String resMsg;

    private Double distance;

    public GzAddrResponse(String resCode, String resMsg, Double distance) {
        this.resCode = resCode;
        this.resMsg = resMsg;
        this.distance = distance;
    }

    public String getResCode() {
        return resCode;
    }

    public void setResCode(String resCode) {
        this.resCode = resCode;
    }

    public String getResMsg() {
        return resMsg;
    }

    public void setResMsg(String resMsg) {
        this.resMsg = resMsg;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "GzAddrResponse{" +
                "resCode='" + resCode + '\'' +
                ", resMsg='" + resMsg + '\'' +
                ", distance=" + distance +
                '}';
    }
}
