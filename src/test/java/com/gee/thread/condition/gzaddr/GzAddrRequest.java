package com.gee.thread.condition.gzaddr;

/**
 * desc:
 *
 * @author gee
 * @since 2021-03-19 09:22:58
 */
public class GzAddrRequest {

    private String lat;

    private String lot;

    public GzAddrRequest(String lat, String lot) {
        this.lat = lat;
        this.lot = lot;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    @Override
    public String toString() {
        return "GzAddrRequest{" +
                "lat='" + lat + '\'' +
                ", lot='" + lot + '\'' +
                '}';
    }
}
