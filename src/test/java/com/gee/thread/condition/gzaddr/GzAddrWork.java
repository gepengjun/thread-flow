package com.gee.thread.condition.gzaddr;

import com.gee.thread.condition.gaode.GaoDeRequest;
import com.gee.thread.condition.gaode.GaoDeWork;
import com.gee.thread.flow.translator.V2PTranslator;
import com.gee.thread.flow.work.AbstractWork;

import java.util.Random;

/**
 * desc:
 *
 * @author gee
 * @since 2021-03-19 09:20:21
 */
public class GzAddrWork<V> extends AbstractWork<V, GzAddrRequest, GzAddrResponse> {

    private String id;

    @Override
    public GzAddrResponse process(GzAddrRequest param) throws Exception {
        Thread.sleep(1000);
        return new GzAddrResponse("0", "调用成功", 3001.98);
    }

    @Override
    public String getId() {
        return id;
    }

    public GzAddrWork(String id) {
        this.id = id;
    }

    public static <V> GzAddrWork<V> build(String id, V2PTranslator<V, GzAddrRequest> v2PTranslator){
        GzAddrWork<V> instance = new GzAddrWork<>(id);
        instance.setV2PTranslator(v2PTranslator);
        return instance;
    }
}
