package com.gee.thread.flow.handler;

import com.gee.thread.flow.common.exception.SkippedException;
import com.gee.thread.flow.common.result.CheckResult;
import com.gee.thread.flow.common.result.ExecuteState;
import com.gee.thread.flow.executor.ExecuteContext;
import com.gee.thread.flow.work.AbstractWork;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * desc:
 *
 * @author gee wrote on  2021-01-14 19:52:51
 */
public class DefaultWorkHandler<V> extends AbstractWorkHandler<V> {

    /**
     * check the execute state of the previous to decide to execute or direct to be exceptional
     * @param currentWorkId the current work's id
     * @param currentExecuteState the current work's execute state
     * @param preWorks  the current work's previous works
     * @param executeContext the execute context
     * @return CheckResult
     */
    @Override
    public CheckResult checkPreWorks(String currentWorkId, AtomicInteger currentExecuteState,
                                     Map<String, AbstractWork<V, ?, ?>> preWorks,
                                     ExecuteContext<V> executeContext) {
        if (preWorks.size() > 0) {

            AbstractWork<V, ?, ?> work = oneOfNecessaryException(currentWorkId, preWorks, executeContext);
            if (work != null && updExecuteStateInit2Working(currentExecuteState)){
                SkippedException skippedException = new SkippedException(PREVIOUS_NECESSARY_EXCEPTION + work.getId());
                return CheckResult.build(ExecuteState.EXCEPTIONAL, skippedException);
            }

            boolean allUnnecessaryException = allUnnecessaryException(currentWorkId, preWorks, executeContext);
            if (allUnnecessaryException
                    && updExecuteStateInit2Working(currentExecuteState)){
                String message = ALL_PREVIOUS_UNNECESSARY_EXCEPTION + getAllPreUnnecessaryIds(currentWorkId, preWorks);
                SkippedException skippedException = new SkippedException(message);
                return CheckResult.build(ExecuteState.EXCEPTIONAL, skippedException);
            }

            boolean anyNecessaryUnfinished = anyNecessaryUnfinished(currentWorkId, preWorks, executeContext);
            /*
                the current work decides not to execute or be exceptional's situations:
                1. the current work has unfinished work which is necessary and if he current work has previous
                   unnecessary works
                2.  he current work has previous unnecessary works, and none of them executes successfully,
                    and exist at least one which is unfinished of previous unnecessary works
             */
            if (anyNecessaryUnfinished
                    || (anyUnnecessaryUnfinished(currentWorkId, preWorks,executeContext)
                    && noneUnnecessarySuccess(currentWorkId, preWorks, executeContext))){
                return CheckResult.build(ExecuteState.INIT);
            }
        }
        return updExecuteStateInit2Working(currentExecuteState)
                ? CheckResult.build(ExecuteState.WORKING) : CheckResult.build(ExecuteState.INIT);
    }

    @Override
    public void postBeforeBegin(String currentWorkId, Object param){
    }

    @Override
    protected void postAfterFinishBeforeNextBegin(AbstractWork<V,?,?> currentWork, ExecuteState currentWorkExecuteState,
                                                  ExecuteContext<V> executeContext){

    }

    @Override
    protected void postAfterFinishAfterNextBegin(AbstractWork<V,?,?> currentWork, ExecuteContext<V> executeContext) {
    }

}
