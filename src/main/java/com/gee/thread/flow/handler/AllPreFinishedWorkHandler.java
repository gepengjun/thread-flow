package com.gee.thread.flow.handler;

import com.gee.thread.flow.common.result.CheckResult;
import com.gee.thread.flow.common.result.ExecuteState;
import com.gee.thread.flow.executor.ExecuteContext;
import com.gee.thread.flow.work.AbstractWork;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * desc:
 *
 * @author gee
 * @since 2021-02-19 14:12:54
 */
public class AllPreFinishedWorkHandler<V> extends DefaultWorkHandler<V> {
    @Override
    public CheckResult checkPreWorks(String currentWorkId, AtomicInteger currentExecuteState,
                                     Map<String, AbstractWork<V, ?, ?>> preWorks, ExecuteContext<V> executeContext) {
        if (preWorks.size() > 0) {
            if (anyPreUnfinished(preWorks, executeContext)) {
                return CheckResult.build(ExecuteState.INIT);
            }
        }
        return this.updExecuteStateInit2Working(currentExecuteState)
                ? CheckResult.build(ExecuteState.WORKING) : CheckResult.build(ExecuteState.INIT);
    }
}
