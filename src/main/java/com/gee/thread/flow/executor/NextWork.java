package com.gee.thread.flow.executor;

import com.gee.thread.flow.work.AbstractWork;

/**
 * desc:
 *
 * @author gee
 * @since 2021-03-16 16:01:33
 */

public class NextWork<V,R> {

    private AbstractWork<V,?,?> work;

    private Boolean necessaryForNext;

    private Condition<R> condition;

    private NextWork(AbstractWork<V, ?, ?> work, boolean necessaryForNext, Condition<R> condition) {
        this.work = work;
        this.necessaryForNext = necessaryForNext;
        this.condition = condition;
    }

    public AbstractWork<V, ?, ?> getWork() {
        return work;
    }

    public void setWork(AbstractWork<V, ?, ?> work) {
        this.work = work;
    }

    public Boolean getNecessaryForNext() {
        return necessaryForNext;
    }

    public void setNecessaryForNext(Boolean necessaryForNext) {
        this.necessaryForNext = necessaryForNext;
    }

    public Condition<R> getCondition() {
        return condition;
    }

    public void setCondition(Condition<R> condition) {
        this.condition = condition;
    }

    public static <V,R> NextWork<V,R> build(AbstractWork<V,?,?> work){
        return new NextWork<>(work, true, new Condition<>());
    }

    public static <V,R> NextWork<V,R> build(AbstractWork<V,?,?> work, boolean necessaryForNext){
        return new NextWork<>(work, necessaryForNext, new Condition<>());
    }

    public static <V,R> NextWork<V,R> build(AbstractWork<V,?,?> work, Condition<R> condition){
        return new NextWork<>(work, true, condition);
    }

    public static <V,R> NextWork<V,R> build(AbstractWork<V,?,?> work, boolean necessaryForNext,
                                            Condition<R> condition){
        return new NextWork<>(work, necessaryForNext, condition);
    }
}
