package com.gee.thread.flow.executor;

import com.gee.thread.flow.common.result.ExecuteState;

/**
 * desc:
 *    根据本单元执行情况, executeState, r 来判断本单元对于下一单元是否通过
 *    对于同一种的Condition而言, 可以实现单例, 减少对象的创建
 * @author gee
 * @since 2021-03-16 16:03:56
 */
public class Condition<R> {

     public boolean determine(int executeState, R r) {
        return executeState == ExecuteState.SUCCESSFUL.getCode();
    }
}
