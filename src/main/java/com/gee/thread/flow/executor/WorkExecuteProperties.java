package com.gee.thread.flow.executor;

import com.gee.thread.flow.common.result.CheckResult;
import com.gee.thread.flow.work.AbstractWork;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;

/**
 * desc:
 *   封装工作单元执行时的属性
 * @author gee wrote on  2021-01-20 08:49:26
 */
public class WorkExecuteProperties<V,R> {

    private Map<String, NextWork<V,R>> nextWorks = new HashMap<>();

    private Map<String, AbstractWork<V,?,?>> preWorks = new HashMap<>();

    /**
     *  前置任务有非必须的单元时, 此属性记录前置任务第一个非必须满足成功条件的单元id
     */
    private AtomicReference<String> preFUSWorkId = new AtomicReference<>();
    /**
     *  记录启动本单元的前置单元id
     */
    private AtomicReference<String> preWorkId = new AtomicReference<>();

    private CheckResult checkResult;

    private CompletableFuture<ExecuteContext<V>> completableFuture;

    private Thread selfThread;

    private Thread asyncThread;

    private Boolean enableInterrupted = false;

    public Map<String, NextWork<V,R>> getNextWorks() {
        return nextWorks;
    }

    public Map<String, AbstractWork<V,?,?>> getPreWorks() {
        return preWorks;
    }

    public AtomicReference<String> getPreWorkId() {
        return preWorkId;
    }

    public CheckResult getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(CheckResult checkResult) {
        this.checkResult = checkResult;
    }

    public CompletableFuture<ExecuteContext<V>> getCompletableFuture() {
        return completableFuture;
    }

    public void setCompletableFuture(CompletableFuture<ExecuteContext<V>> completableFuture) {
        this.completableFuture = completableFuture;
    }

    public Thread getSelfThread() {
        return selfThread;
    }

    public void setSelfThread(Thread selfThread) {
        this.selfThread = selfThread;
    }

    public Thread getAsyncThread() {
        return asyncThread;
    }

    public void setAsyncThread(Thread asyncThread) {
        this.asyncThread = asyncThread;
    }

    public Boolean getEnableInterrupted() {
        return enableInterrupted;
    }

    public void setEnableInterrupted(Boolean enableInterrupted) {
        this.enableInterrupted = enableInterrupted;
    }

    public AtomicReference<String> getPreFUSWorkId() {
        return preFUSWorkId;
    }

    public void setPreFUSWorkId(AtomicReference<String> preFUSWorkId) {
        this.preFUSWorkId = preFUSWorkId;
    }
}
