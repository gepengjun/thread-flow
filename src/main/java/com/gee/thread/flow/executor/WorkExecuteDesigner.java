package com.gee.thread.flow.executor;

import com.gee.thread.flow.handler.DefaultWorkHandler;
import com.gee.thread.flow.handler.WorkHandler;
import com.gee.thread.flow.work.AbstractWork;
import com.gee.thread.flow.work.impl.EndWork;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * desc:
 *
 * @author gee wrote on  2021-01-16 20:08:56
 */
public class WorkExecuteDesigner<V> {

    private Set<AbstractWork<V,?,?>> startWorkSet;

    private V variable;

    private EndWork<V> endWork;

    private WorkExecuteDesigner(Set<AbstractWork<V,?,?>> startWorkSet, V variable, EndWork<V> endWork) {
        this.startWorkSet = startWorkSet;
        this.variable = variable;
        this.endWork = endWork;
    }

    public Set<AbstractWork<V,?,?>> getStartWorkSet() {
        return startWorkSet;
    }

    public V getVariable() {
        return variable;
    }

    public void setEndWork(EndWork<V> endWork) {
        this.endWork = endWork;
    }

    public EndWork<V> getEndWork() {
        return endWork;
    }

    public static <V> WorkExecutorDesignerBuilder<V> builder(){
        return new WorkExecutorDesignerBuilder<V>();
    }

    public static class WorkExecutorDesignerBuilder<V> {

        private AbstractWork<V,?,?> currentWork;

        private Set<AbstractWork<V,?,?>> startWorkSet = new HashSet<>();

        private V variable;

        private EndWork<V> endWork;

        private WorkHandler<V> commonWorkHandler;

        public WorkExecutorDesignerBuilder<V> variable(V variable){
            this.variable = variable;
            this.endWork = Optional.ofNullable(endWork).orElse(EndWork.build(null));
            return this;
        }

        public WorkExecutorDesignerBuilder<V> newStartWork(AbstractWork<V,?,?> work){
            assert work != null;
            this.currentWork = work;
            commonWorkHandler = Optional.ofNullable(commonWorkHandler).orElse(new DefaultWorkHandler<>());
            if (!Optional.ofNullable(currentWork.getWorkHandler()).isPresent()){
                currentWork.setWorkHandler(commonWorkHandler);
            }
            startWorkSet.add(work);
            return this;
        }

        public WorkExecutorDesignerBuilder<V> currentWork(AbstractWork<V,?,?> work){
            assert currentWork != null;
            assert work != null;
            this.currentWork = work;
            return this;
        }

        public WorkExecutorDesignerBuilder<V>  handler(WorkHandler<V> workHandler){
            assert currentWork != null;
            this.currentWork.setWorkHandler(workHandler);
            return this;
        }

        public WorkExecutorDesignerBuilder<V>  commonHandler(WorkHandler<V> workHandler){
            assert workHandler != null;
            this.commonWorkHandler = workHandler;
            return this;
        }

        public WorkExecutorDesignerBuilder<V>  endLine(boolean necessaryForEnd,Condition<?> condition){
            condition = Optional.ofNullable(condition).orElse(new Condition<>());
            NextWork nextWork = NextWork.build(this.endWork, necessaryForEnd, condition);
            currentWork.getWorkExecuteProperties().getNextWorks().put(endWork.getId(), nextWork);
            endWork.getWorkExecuteProperties().getPreWorks().put(this.currentWork.getId(),this.currentWork);
            return this;
        }

        public WorkExecutorDesignerBuilder<V> next(boolean necessary, AbstractWork<V,?,?> work, Condition<?> condition){
            commonWorkHandler = Optional.ofNullable(commonWorkHandler).orElse(new DefaultWorkHandler<>());
            if (!Optional.ofNullable(work.getWorkHandler()).isPresent()){
                work.setWorkHandler(commonWorkHandler);
            }
            condition = Optional.ofNullable(condition).orElse(new Condition<>());
            NextWork nextWork = NextWork.build(work, necessary, condition);
            currentWork.getWorkExecuteProperties().getNextWorks().put(work.getId(), nextWork);
            work.getWorkExecuteProperties().getPreWorks().put(this.currentWork.getId(),this.currentWork);
            return this;
        }

        public WorkExecutorDesignerBuilder<V> enableInterrupted(boolean enableInterrupted){
            assert currentWork != null;
            currentWork.getWorkExecuteProperties().setEnableInterrupted(enableInterrupted);
            return this;
        }

        public WorkExecuteDesigner<V> build(){
            return new WorkExecuteDesigner<V>(startWorkSet, variable,endWork);
        }

    }
}
