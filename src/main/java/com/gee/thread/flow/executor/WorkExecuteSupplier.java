package com.gee.thread.flow.executor;

import java.util.concurrent.*;
import java.util.function.Supplier;

/**
 * desc:
 * @author gee wrote on  2021-01-14 20:09:09
 */
public class WorkExecuteSupplier<R> implements Supplier<R> {

    private CompletableFuture<R> executeContextCompletableFuture;

    private CountDownLatch countDownLatch = new CountDownLatch(1);

    public WorkExecuteSupplier(CompletableFuture<R> executeContextCompletableFuture) {
        this.executeContextCompletableFuture = executeContextCompletableFuture;
    }

    @Override
    public R get() {
        R r = null;
        try {
            r = executeContextCompletableFuture.get(Integer.MAX_VALUE, TimeUnit.MICROSECONDS);
            countDownLatch.await();
        } catch (InterruptedException | ExecutionException | TimeoutException ignored) {
        }
        return r;
    }
    public CountDownLatch getCountDownLatch() {
        return countDownLatch;
    }

}
